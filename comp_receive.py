#-*- coding: latin-1 -*-
import socket
import time
import pickle
import struct
from comp_save_data import sensorData

PICTCPPORT = 22005
IRUDPPORT = 22006
ULTRAUDPPORT = 22007
FLOWUDPPORT = 22008

def receive_all(connect, length):
    data = b''
    while len(data) < length:                                               
        part = connect.recv(length - len(data))
        if not part:
            return None
        data += part
    return data

def receive_message(connect):
    raw_length = receive_all(connect, 4)
    if not raw_length:
        return None
    length = struct.unpack('>I',raw_length)[0]
    return receive_all(connect, length)

def receive_udp(connect):
    return connect.recvfrom(1024)

def getCamera(sock, allData):               
    while True:                           
        msg = receive_message(sock)     
        pic = pickle.loads(msg)
        allData.writePic(pic) 
        
def getThermal(sock, allData):
    while True:                            
        msg = receive_udp(sock)      
        irPic = pickle.loads(msg[0])
        allData.writeThermal(irPic)
    
def getUltrasound(sock, allData): 
    while True:
        msg = receive_udp(sock)      
        multiArray = pickle.loads(msg[0])
        allData.writeUltra(multiArray)     

def getFlow(socket, allData):
    while True:
        msg = receive_udp(socket)
        flow = pickle.loads(msg[0])
        allData.writeFlow(flow)

#Skicka in en referens till en sensorData klass
#Skapar 3 udp sockets och connectar till en TCP socket skapad p� dronaren. Skickar Go nar
# alla sockets ar redo pa att ta emot data
def receiveData(allData, pool_listen, rpi_ip):
    try:
        sockThermal = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)    
        sockThermal.bind(("", IRUDPPORT))    

        sockUltra = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)    
        sockUltra.bind(("", ULTRAUDPPORT))    

        sockFlow = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sockFlow.bind(("", FLOWUDPPORT))    

        sockPic = socket.socket(socket.AF_INET, socket.SOCK_STREAM)    
        sockPic.connect((rpi_ip, PICTCPPORT))
        sockPic.send(socket.gethostbyname(socket.gethostname()).encode())
        sockPic.send('go'.encode())
        #pool_listen.apply_async(getCamera, (sockPic,allData))
        pool_listen.apply_async(getThermal, (sockThermal,allData))
        pool_listen.apply_async(getUltrasound, (sockUltra,allData))
        pool_listen.apply_async(getFlow, (sockFlow, allData))  
    #On exceptions, close all sockets
    except:
        sockPic.close()
        sockThermal.close()
        sockUltra.close()
        #sockFlow.close()
        pool_listen.terminate()
        raise
