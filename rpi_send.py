#-*- coding: latin-1 -*-
import socket
import sys
import pickle
import struct
import time
from multiprocessing.pool import ThreadPool
from multiprocessing import Lock
import multisensor
import thermal
import optical_flow
from ctypes import *
import ctypes
_libcon=ctypes.CDLL('/home/pi/firedrone/dronecontrol.so')

# Har i uppdrag att samla ihop och skicka vidare all sensordata

DEBUG = False

PICTCPPORT = 22005
IRUDPPORT = 22006
ULTRAUDPPORT = 22007
FLOWUDPPORT = 22008
IMGWIDTH = 640
IMGHEIGHT = 480

# Setup pa kameran
def init_camera():
	camera = PiCamera()
	camera.resolution = (IMGWIDTH, IMGHEIGHT)
	camera.framerate = 60
	return camera

def init_thermal():
	return thermal.init_sensor()

def init_optical_flow():
	return optical_flow.init_optical_flow()

def control_drone(saver):
	_libcon.init()
	while True:
		data = saver.getData()
		_libcon.run(c_short(data[2]), c_float(data[0]/1000.0), c_float(data[1]/1000.0))

def read_ultra_data(socket, ip, pool):
	while True:
		distances = multisensor.get_distances_filtered()
		send_udp_data(socket, ip, ULTRAUDPPORT, pickle.dumps(distances))

def read_flow_data(opt_bus, saver, socket, ip):
	while True:
		flowData = optical_flow.update_filtered(opt_bus)
		saver.setData(flowData)
		send_udp_data(socket, ip, FLOWUDPPORT, pickle.dumps(flowData))

def read_thermal_data(thermal_cam, socket, ip, pool):
	while True:
		thermalData = thermal.get_temps(thermal_cam)
		send_udp_data(socket, ip, IRUDPPORT, pickle.dumps(thermalData))

def read_camera_data(camera, socket, ip, pool):
	lock_cam = Lock()
	while True:
		cameraData = camera_drone.capture_image(camera)
		lock_cam.acquire()
		pool.apply_async(send_tcp_data, (socket, cameraData, lock_cam))

def send_udp_data(connect, ip, port, data):
	connect.sendto(data, (ip, port))

# Lagger till meddelandelangden till ett meddelande och picklar osv
def send_tcp_data(connect, data, lock):
	start = time.time()
	pickData = pickle.dumps(data)
	msg = struct.pack('>I', len(pickData)) + pickData
	connect.sendall(msg)
	lock.release()
	if DEBUG:
		print('it took', time.time() - start, 'to send data')

class flow_save:
	dataLock = Lock()
	savedData = (0,0,0)

	def getData(self):
		self.dataLock.acquire()
		data = self.savedData
		self.dataLock.release()
		return data

	def setData(self, data):
		self.dataLock.acquire()
		self.savedData = data
		self.dataLock.release()

#Initiera alla sensorer och tradar
done = False
pool = ThreadPool(processes = 5)
#cam = init_camera()
flo = init_optical_flow()
#the = init_thermal()
saver = flow_save()

#Skapa socket att skicka kameradata over
print('Initializing connection')
sock_cam = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock_cam.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
host = ''
port = PICTCPPORT
sock_cam.bind((host, port))
sock_cam.listen(1)
sockCam_conn, sockCam_compIP = sock_cam.accept()
print('Connected to ', sockCam_compIP)
sock_cam.close()

#Skapa socket att skicka IR-data over
#sock_thermal = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

#Skapa socket att skicka multisensor-data over
sock_ultra = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

#Skapa socket att skicka kameradata over
sock_flow = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

done = False
command = 'nothing'

# Vanta pa ett commando fran datorn att allt ar ok
while not done:
	print('Waiting for command')
	otherIP = sockCam_conn.recv(1024).decode()
	print('Received ip: ', otherIP)
	command = sockCam_conn.recv(1024).decode()
	print('Received command: ', command)
	if (command) == 'exit':
		sys.exit('Exited')
	if (command == 'go'):
		done = True

done = False
try:
	waiter = pool.apply_async(control_drone, (saver,))
	pool.apply_async(read_ultra_data, (sock_ultra, otherIP, pool))
	pool.apply_async(read_flow_data, (flo, saver, sock_flow, otherIP))
#	pool.apply_async(read_thermal_data, (the, sock_thermal, otherIP, pool))
#	pool.apply_async(read_camera_data, (cam, sockCam_conn, otherIP, pool))
	while True:
		pass

finally:
	sockCam_conn.shutdown(socket.SHUT_RDWR)
#	sock_thermal.shutdown(socket.SHUT_RDWR)
	sock_ultra.shutdown(socket.SHUT_RDWR)
	sock_flow.shutdown(socket.SHUT_RDWR)
	pool.terminate()
	raise
