import time
from multiprocessing import Lock
class sensorData:
	picData = 0
	irData = 0
	ultraData = (0.0,0.0,0.0,0.0,0.0,0.0)
	flowData = (0.0,0.0,0)

	picLock = Lock()
	irLock = Lock()
	ultraLock = Lock()
	flowLock = Lock()
#Spara tidsskillnad mellan värdena för multisensor (de som används i if-satsen), samma för skillnad mellan data  
	
	#Write metoderna fungerar enligt foljande: Ta las, spara data, slapp las.
	
	#Uppdatera bilddatan
	def writePic(self, data):
		self.picLock.acquire()
		self.picData = data
		self.picLock.release()
				
	#Uppdatera irdatan
	def writeThermal(self, data):
		self.irLock.acquire()
		self.irData = data
		self.irLock.release()

	#Uppdatera ultraljuds-datan  
	def writeUltra(self, data):
		self.ultraLock.acquire()
		self.ultraData = data
		self.ultraLock.release()
	
	#Uppdatera flowdatan	
	def writeFlow(self, data):
		self.flowLock.acquire()
		self.flowData = data
		self.flowLock.release()
	
	#Get metoderna fungerar enligt foljande: ta las, spara undan data, slapp las, returnerna: (data, tid)
	
	#Hamta senaste bilden
	def getPic(self):
		self.picLock.acquire()
		picTempData = self.picData
		self.picLock.release()
		
		return picTempData
	
	#Hamta senaste ir-bilden	
	def getThermal(self):
		self.irLock.acquire()
		irTempData = self.irData
		self.irLock.release()
		
		return irTempData
	
		#Hamta senaste multisensor1-datan
	def getUltra(self):
		self.ultraLock.acquire()
		ultraTempData = self.ultraData
		self.ultraLock.release()
		
		return ultraTempData

	#Hamta senaste flowdatan
	def getFlow(self):
		self.flowLock.acquire()
		flowTempData = self.flowData
		self.flowLock.release()
		
		return flowTempData
