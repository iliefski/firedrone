import multisensor
import time
import os
import RPi.GPIO as GPIO

#Tests all sensors with this pin setup from left to right:
#gpioLeft = (7,11)
#gpioCL1 = 13
#gpioCL2 = 15
#gpioCR2 = 12
#gpioCR1 = 16
#gpioRight = (18,22)
def test_all():
	try:
		file = open("test_multisensor_data.txt", 'w')
		starttime = time.time()
		while 1:
			distances = multisensor.get_distances()
			os.system('cls' if os.name == 'nt' else 'clear')
			print('Distances, left to right, {: 06.2f}, {: 06.2f}, {: 06.2f}, {: 06.2f}, {: 06.2f}, {: 06.2f}'.format(distances[0], distances[1], distances[2], distances[3], distances[4], distances[5]))
			file.write(str(time.time() - starttime) + ',' + str(distances[0]) + ',' +str(distances[1]) + ',' +str(distances[2]) + ',' +str(distances[3]) + ',' +str(distances[4]) + ',' +str(distances[5]) + '\n')
	except:
		file.close()
		raise

def test_filtered():
	try:
		file = open("test_multisensor_data.txt", 'w')
		starttime = time.time()
		while 1:
			distances = multisensor.get_distances_filtered()
			os.system('cls' if os.name == 'nt' else 'clear')
			print('Distances, left to right, {: 06.2f}, {: 06.2f}, {: 06.2f}, {: 06.2f}, {: 06.2f}, {: 06.2f}'.format(distances[0], distances[1], distances[2], distances[3], distances[4], distances[5]))
			file.write(str(time.time() - starttime) + ',' + str(distances[0]) + ',' +str(distances[1]) + ',' +str(distances[2]) + ',' +str(distances[3]) + ',' +str(distances[4]) + ',' +str(distances[5]) + '\n')
	except:
		file.close()
		raise

#Tests one PING sensor
def test_PING(pin):
	try:
		file = open("test_multisensor_data.txt", 'w')
		starttime = time.time()
		while 1:
			GPIO.setmode(GPIO.BOARD)
			distances = multisensor.test_distance_PING(pin)
			os.system('cls' if os.name == 'nt' else 'clear')
			print("Distance, pin " + str(pin) + ": ", distances)
			file.write(str(time.time() - starttime) + ',' + str(distances) + '\n')
			time.sleep(0.01)
	except:
		raise
		file.close()

#Test one SR04 sensor
def test_SR04(trigPin, echoPin):
	try:
		file = open("test_multisensor_data.txt", 'w')
		starttime = time.time()
		while 1:
			GPIO.setmode(GPIO.BOARD)
			distances = multisensor.test_distance_SR04(trigPin, echoPin)
			os.system('cls' if os.name == 'nt' else 'clear')
			print("Distance, pin " + str(trigPin) + ": ", distances)
			file.write(str(time.time() - starttime) + ',' + str(distances) + '\n')
			time.sleep(0.05)
	except:
		raise
		file.close()

#test_all()
#test_PING(15)
test_filtered()
#test_SR04(7,11)
