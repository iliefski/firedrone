import java.util.*;
import java.io.*;

public class extract{

    public static void write() throws Exception{
	PrintWriter writer = new PrintWriter("the-file-name.txt", "UTF-8");
	writer.println("The first line");
	writer.println("The second line");
	writer.close();
    }
    
    public static void main(String[] args) throws Exception{
	Scanner scanner = new Scanner(new File(args[0]));
	String line = null;
	String list[] = null;

	//	write();
	PrintWriter writerTime = new PrintWriter("dataToMatlab.txt", "UTF-8");

	while(scanner.hasNext()){
	    line = scanner.nextLine();
	    line = line.replaceAll("[^0-9 ^. ^-]+", " ");
	    list = line.trim().split(" ");
	    writerTime.print(list[0] + " ");

	    line = scanner.nextLine();
	    line = line.replaceAll("[^0-9 ^. ^-]+", " ");
	    list = line.trim().split(" ");
	    
	    writerTime.print(list[0] + " " + list[2] + " " + list[4] + " " + list[6] + " " + list[8] + " " + list[10] + " ");
	    
	    line = scanner.nextLine();
	    line = line.replaceAll("[^0-9 ^. ^-]+", " ");
	    list = line.trim().split(" ");

	    writerTime.print(list[0] + " " + list[2] + " " + list[4] + " ");
	    
	    scanner.nextLine();
	    scanner.nextLine();
	    
	    line = scanner.nextLine();
	    line = line.replaceAll("[^0-9 ^. ^-]+", " ");
	    list = line.trim().split(" ");

	    writerTime.print(list[0] + " " + list[2] + " " + list[4] + "\n");

	    scanner.nextLine();
  
	}
	writerTime.close();
    }
}
