'''Delete all this (it is copied as a test)'''
from picamera import PiCamera
from time import sleep
from io import BytesIO
from PIL import Image
import numpy as np

def capture_image(camera):
	streamCam = BytesIO()

	'''for i in range(3):
	sleep(2)
	camera.capture('/home/pi/Desktop/streamSocket%s' % i, 'png')'''

	sleep(0.01)
	camera.capture(streamCam, 'png', use_video_port=True)
	streamCam.seek(0)
	camPic = Image.open(streamCam)

	#Tried to do direct raw rgb img
	#camPic = np.empty((640, 480, 3), dtype = np.uint8)
	#camera.capture(camPic, 'rgb')

	'''Used for actually reading the data'''
	'''camPic.load()'''

	return camPic


