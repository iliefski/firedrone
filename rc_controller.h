#ifndef RC_CONTROLLER_H
#define RC_CONTROLLER_H

void RC_init();

int getRCThrottle();
int getRCYaw();
int getRCPitch();
int getRCRoll();
int getRCManual();

#endif // RC_CONTROLLER_H
