#include "control_signal.h"
#include <math.h>
// PINS FÖR UTSIGNALER
#define ROLL 26
#define PITCH 19
#define THROTTLE 13
#define YAW 6
#define SERVO 5

// Max och minvärde för olika operationer
#define ROLL_MIN -100
#define ROLL_MAX 100
#define PITCH_MIN -100
#define PITCH_MAX 100
#define THROTTLE_MIN 0
#define THROTTLE_MAX 100
#define YAW_MIN -100
#define YAW_MAX 100
#define SERVO_MIN -100
#define SERVO_MAX 100
#define MAX_ANGLE 0.35f
#define SIN_MAX_ANGLE 0.34f

// De olika utsignalernas pulslängd i mikrosekunder
static int roll_PW = 1500;
static int pitch_PW = 1500;
static double throttle_PW = 1000.0;
static int yaw_PW = 1500;
static int servo_PW = 1500;

// Regulatorparametrar
#define MASS 1.344f
#define G 9.82f
#define MAX_FORCE 3.3f

#define ROLL_OFFSET -10.0f
#define PITCH_OFFSET -9.0f
// För throttle
static float Kpthr = 0.01f;
static float Kdthr = 3.0f;
static float Tfthr = 1.0f;
static float Kithr = 0.5f;
static float Ithr = 0.0f;
static float Dthr = 0.0f;
static float Pthr = 0.0f;
static float e_gammalthr = 0.0f;
// För roll
static float Kproll = 2.0f;
static float Kiroll = 2.0f;
static float Proll = 0.0f;
static float Iroll = 0.0f;

// För pitch
static float Kppitch = 2.0f;
static float Kipitch = 2.0f;
static float Ppitch = 0.0f;
static float Ipitch = 0.0f;

// Filterparametrar
static float a0 = 2.0160f;
static float a1 = -1.9840f;
static float b0 = 0.0160f;
static float b1 = 0.0160f;

static float Fz = 1.0f;
static float Fxymax = 1.0f;
static float w0 = 1.0f;

typedef int bool;
enum{false, true};

bool drone_running;

void updateRoll(float ref, float velx, float h){
  float e = ref-velx;

  Proll = Kproll*e;
  float Fx = Proll + Iroll;
  Fxymax = Fz*SIN_MAX_ANGLE;
  if(Fx>Fxymax){
    Fx = Fxymax;
  }
  float theta = asin(Fx/Fz);
  float rollf = theta*100/MAX_ANGLE;
  printf("Rollf: %f, Vely: %f\n", rollf, velx);
  setRoll(rollf);
  Iroll = Iroll + e*h*Kiroll;
}

void updatePitch(float ref, float vely, float h){
  float e = ref-vely;

  Ppitch = Kppitch*e;
  float Fy = Ppitch + Ipitch;
  Fxymax = Fz*SIN_MAX_ANGLE;
  if(Fy>Fxymax){
    Fy = Fxymax;
  }
  float theta = asin(Fy/Fz);
  float pitchf = theta*100/MAX_ANGLE;
  printf("Pitchf: %f, Velx: %f\n", pitchf, vely);
  setRoll(pitchf);
  Ipitch = Ipitch + e*h*Kipitch;
}

static float filterHeight(float x){
  static float yold = 0;
  static float xold = 0;
  float y = (b1*xold + b0*x - a1*yold)/a0;
  xold = x;
  yold = y;
  return y;
}

void updateThrottle(float ref, float height, float h){
  static float htot = 0;
  static float lastHeight = 0;
  htot += h;
  if(height < 0){
        printf("Height cannot be less than 0\n");
    return;
  }
  if((fabsf(height - lastHeight)) > 0.5){
     printf("To large difference in height from last sample. H:%f, LH:%f\n", height, lastHeight);
    if(height < 2.99f) {
	lastHeight = lastHeight + 0.15*(height-lastHeight);
	height = lastHeight;
    } else {
        height = lastHeight;
    }
  }
//   printf("height = %f \n", height);
//   printf("ref = %f (true) \n", ref);
  ref = filterHeight(ref);
//  printf("ref = %f (filtrerad) \n", ref);

  float e = ref-height;
  
  Pthr = Kpthr*e;
  Dthr = (Tfthr/(Tfthr + htot) * Dthr) + (Kdthr/(Tfthr + htot)*(e - e_gammalthr));
  float Ftot = Pthr + Ithr + Dthr;
  
//   printf("Ftot = %f \n", Ftot);
  float Fdrone = Ftot + MASS*G;
  Fz = Fdrone;
  //printf("Fdrone = %f \n", Fdrone);
  float thrf = Fdrone*100/(MAX_FORCE*G);
//  printf("thrf = %f \n", thrf);
  // printf("Samplingsintervall: %f s \n", htot);
  // Test prints
  printf("Ftot: %05.2f, height: %05.2f, fref: %05.2f, thrf: %05.2f\n", Ftot, height, ref, thrf);

  setThrottle(thrf);
  Ithr = Ithr + e*htot*Kithr;
  e_gammalthr = e;
  lastHeight = height;
  htot = 0;
}

// Sätter rolls pulslängd (input -100 till 100)
void setRoll(float rol){
  
  if(rol > ROLL_MAX){
    rol = ROLL_MAX;
  }
  if(rol < ROLL_MIN){
    rol = ROLL_MIN;
  }
  double rold = (double) rol;
  roll_PW = (int) 1500 + (rold/100.0) * 500;
  gpioServo(ROLL, roll_PW);
  //  printf("Roll PW set to %d \n", roll_PW);
}

// Returnerar roll
int getRoll(void){
  return ((roll_PW-1500)/500 * 100);
}

// sätter pitch pulslängd (input -100 till 100)
void setPitch(float pit){
  if(pit > PITCH_MAX){
    pit = PITCH_MAX;
  }
  if(pit < PITCH_MIN){
    pit = PITCH_MIN;
  }
  double pitd = (double) pit;
  pitch_PW = (int) 1500 - (pitd/100) * 500;
  gpioServo(PITCH, pitch_PW);
  //  printf("Pitch PW set to %d \n", pitch_PW);
}
// Returnerar pitch
int getPitch(void){
  return ((pitch_PW-1500)/500 * 100);
  
}


// sätter throttle pulslängd (input 0 till 100)
void setThrottle(double thr){
  //printf("Setting throttle: %f", thr);
  if(thr > THROTTLE_MAX){
    thr = THROTTLE_MAX;
  }
  if(thr < THROTTLE_MIN){
    thr = THROTTLE_MIN;
  }
  double pw = 1000 + thr*10;
  throttle_PW = pw;
  gpioServo(THROTTLE, (int) throttle_PW);
  //printf("throttle PW set to %d \n", throttle_PW);
}

// Returnerar throttle
double getThrottle(void){
  return throttle_PW;
}

// sätter yaw pulslängd (input -100 till 100)
void setYaw(float yaw){
  if(yaw > PITCH_MAX){
    yaw = YAW_MAX;
  }
  if(yaw < PITCH_MIN){
    yaw = YAW_MIN;
  }
  double yawd = (double) yaw;
  yaw_PW = (int) 1500 + (yawd/100) * 500;
  gpioServo(YAW, yaw_PW);
  //  printf("Yaw PW set to %d \n", yaw_PW);
}
// Returnerar Yaw
int getYaw(void){
  return ((yaw_PW-1500)/500 * 100);
}


// sätter servo pulslängd (input -100 till 100)
void setServo(int ser){
  if(ser > SERVO_MAX){
    ser = SERVO_MAX;
  }
  if(ser < SERVO_MIN){
    ser = SERVO_MIN;
  }
  double serd = (double) ser;
  servo_PW = 1500 + (serd/100) * 500;
  gpioServo(SERVO, servo_PW);
  // printf("Servo PW set to %d \n", servo_PW);
}
// Returnerar servo
int getServo(void){
  return ((servo_PW-1500)/500 * 100);
}

// Sätter pinnar till utportar
void GPIO_init(float T){
    // Initiera GPIO
  if(gpioInitialise() == PI_INIT_FAILED){
    printf("GPIO Failed to init \n");
    return;
  }
  else{
    printf("GPIO succesfully initialised \n");
      
  }

  gpioSetMode(ROLL, PI_OUTPUT);
  gpioSetMode(PITCH, PI_OUTPUT);
  gpioSetMode(THROTTLE, PI_OUTPUT);
  gpioSetMode(YAW, PI_OUTPUT);
  gpioSetMode(SERVO, PI_OUTPUT);

  a0 = T*w0 + 2;
  a1 = T*w0 - 2;
  b0 = T*w0;
  b1 = T*w0; 
}

// Blockerande function som armar motorerna
void armMotors(void){
  printf("Påbörjar att arma motorer \n");
  setThrottle(-100);
  setYaw(100);
  sleep(7);
  setYaw(0);
  printf("Klar \n");

}

// Blockerande function som disarmar motorerna
void disarmMotors(void){
  printf("Påbörjar disarm av motorer \n");
  setThrottle(-100);
  setYaw(-100);
  sleep(7);
  setYaw(0);
  printf("klar \n");

}

void startOutput(void){
  drone_running = true;
  gpioServo(ROLL, roll_PW);
  gpioServo(PITCH, pitch_PW);
  gpioServo(THROTTLE, throttle_PW);
  gpioServo(YAW, yaw_PW);
  gpioServo(SERVO, servo_PW);
  printf("Påbörjar sändning till drönare \n");
}

void stopOutput(void){
  printf("Avslutar sändning till drönare \n");
  drone_running = false;
  gpioTerminate();
  
}
