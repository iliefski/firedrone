import time
import RPi.GPIO as GPIO
import math
import statistics

#Usage: set the pin number to where you connected the Ping))) sensors
#	and then call get_distances()
#Output: a tuple containing distance in cm for (left, center, right, down)
#	sensors. Returns -1 if the distnce is above 3m since those distances
#	are unreliable.

#Pin number not GPIO number
#if HCSR04 sensor, then use tuple instead
gpioLeft = (7,11)
gpioCL1 = 13
gpioCL2 = 15
gpioCR2 = 12
gpioCR1 = 16
gpioRight = (18,22)
maxdistance = 223
filter_length = 3
value_index = 0
values = [[0 for i in range(filter_length)] for _ in range(6)]

def measure_distance(pin):
	begintime = time.time()
	while GPIO.input(pin) == 0:
		begintime = time.time()

	endtime = time.time()
	while GPIO.input(pin) == 1:
		endtime = time.time()

	totaltime = endtime - begintime;

	distance = totaltime * 34300 / 2

	if distance > maxdistance:
		distance = maxdistance

	return distance


def test_distance_PING(gpioPin):
	GPIO.setup(gpioPin, GPIO.OUT)

	GPIO.output(gpioPin, False)	#Set output to low to prepare for signal
	time.sleep(0.000002)
	GPIO.output(gpioPin, True)	#Set output to high to send signal
	time.sleep(0.000005)
	GPIO.output(gpioPin, False)	#Set output to low to signal done

	GPIO.setup(gpioPin, GPIO.IN)

	return measure_distance(gpioPin)


def test_distance_SR04(outPin, inPin):
	GPIO.setup(outPin, GPIO.OUT)

	GPIO.setup(inPin, GPIO.IN)

	GPIO.output(outPin,False)
	time.sleep(0.00002)
	GPIO.output(outPin,True)
	time.sleep(0.00001)
	GPIO.output(outPin,False)

	return measure_distance(inPin)

# Returns a tuple with the distances measured in series.
# (from left to right in order)
# A distance of above 244cm returns as 244cm
def get_distances():
	try:
		GPIO.setmode(GPIO.BOARD)
		distLeft = test_distance_SR04(gpioLeft[0], gpioLeft[1])
		distCL1 = test_distance_PING(gpioCL1)
		distCL2 = test_distance_PING(gpioCL2)
		distCR2 = test_distance_PING(gpioCR2)
		distCR1 = test_distance_PING(gpioCR1)
		distRight = test_distance_SR04(gpioRight[0], gpioRight[1])
	finally:
		GPIO.cleanup()
	return [distLeft, distCL1, distCL2, distCR2, distCR1, distRight]

# Returns a tuple with the filtered distances measured in series.
# (from left to right in order)
# A distance of above 244cm returns as 244cm
def get_distances_filtered():
	global value_index
	try:
		GPIO.setmode(GPIO.BOARD)
		distLeft = filter_dist(test_distance_SR04(gpioLeft[0], gpioLeft[1]), 0)
		distCL1 = filter_dist(test_distance_PING(gpioCL1), 1)
		distCL2 = filter_dist(test_distance_PING(gpioCL2), 2)
		distCR2 = filter_dist(test_distance_PING(gpioCR2), 3)
		distCR1 = filter_dist(test_distance_PING(gpioCR1), 4)
		distRight = filter_dist(test_distance_SR04(gpioRight[0], gpioRight[1]), 5)
		value_index = value_index + 1
	finally:
		GPIO.cleanup()
	return [distLeft, distCL1, distCL2, distCR2, distCR1, distRight]

def filter_dist(dist, index):
	global value_index
	values[index][value_index%filter_length] = dist
	if value_index < filter_length:
		return dist
	return statistics.median(values[index])

def get_distances_threaded(pool):
	try:
		GPIO.setmode(GPIO.BOARD)
		handle1 = pool.apply_async(test_distance_PING, (gpioLeft,))
		handle2 = pool.apply_async(test_distance_PING, (gpioCR2,))
		distLeft = handle1.get()
		distCR1 = handle2.get()
		handle1 = pool.apply_async(test_distance_PING, (gpioCL1,))
		handle2 = pool.apply_async(test_distance_PING, (gpioCR1,))
		distCL1 = handle1.get()
		distCR1 = handle2.get()
		handle1 = pool.apply_async(test_distance_PING, (gpioCL2,))
		handle2 = pool.apply_async(test_distance_PING, (gpioRight,))
		distCL2 = handle1.get()
		distRight = handle2.get()
	finally:
		GPIO.cleanUup()
	return [distLeft, distCL1, distCL2, distCR2, distCR1, distRight]

class SpeedMeasure:

	def __init__(self):
		self.prevTime = -1
		self.prevDistances = [-1, -1, -1, -1]

	# Returns approximate speeds and distances in cm/sec
	#  or -1 if invalid measure
	def measure(self):
		speeds = [-1,-1,-1,-1]
		newTime = time.time()
		newDistances = get_distances()

		if self.prevTime == -1:
			self.prevDistances = newDistances
			self.prevTime = newTime
			return -1

		timeDif = newTime - self.prevTime

		for i in range(0,3):
			if self.prevDistances[i] > -1 and newDistances[i] > -1 and math.fabs(self.prevDistances[i] - newDistances[i]) < 50:
				speeds[i] = (self.prevDistances[i] - newDistances[i])/timeDif

		self.prevDistances = newDistances
		self.prevTime = newTime;

		return [newDistances, speeds]

	#Only for testing!!!
	def measure1(self, index):
		speed = -1
		newTime = time.time()
		GPIO.setmode(GPIO.BOARD)
		newDistance = test_distance_PING(gpioLeft)
		GPIO.cleanup()

		if self.prevTime == -1:
			self.prevDistances[index] = newDistance
			self.prevTime = newTime
			return -1

		timeDif = newTime - self.prevTime

		if self.prevDistances[index] > -1 and newDistance > -1 and math.fabs(self.prevDistances[index] - newDistance) < 50:
			speed = (self.prevDistances[index] - newDistance)/timeDif

		self.prevDistances[index] = newDistance
		self.prevTime = newTime
