#include <stdio.h>
#include "control_signal.h"

int main(){
  GPIO_init();

  startOutput();
  setThrottle(100);
  
  sleep(20);

  setThrottle(0);
  sleep(10);
  stopOutput();
}
