/* I denna fil finns samtliga funktioner som behövs
   för att ge styrsignaler till drönaren och till 
   servo-motorn. 
   
   Totalt används 6 pthreads
   
  1. Output måste aktiveras med GPIO_init()
  2. Starta styrsignaler med startOutput()
  3. Arma motorer med armMotors() OBS! Blockerar i 7 sekunder!
  4. Använd set-kommando för att ställa hastigheter 
  (-100 är maximal negativ och 100 är maximal positiv)
  5. Disarma motorer med disarmMotors() OBS! Blockerar i 7 sekunder!
  6. Använd stopOutput() för att avsluta sändning. 
  funktionen avslutar även alla interna threads*/

#ifndef CONTROL_SIGNAL_H
#define CONTROL_SIGNAL_H

#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <pigpio.h>
#include <math.h>

void updateThrottle(float ref, float height, float h);
void updateRoll(float ref, float velx, float h);
void updatePitch(float ref, float vely, float h);

void setRoll(float rol);
int getRoll(void);
void setPitch(float pit);
int getPitch(void);
void setThrottle(double thr);
double getThrottle(void);
void setYaw(float yaw);
int getYaw(void);
void setServo(int ser);
int getServo(void);

void GPIO_init(float T);

void armMotors(void);
void disarmMotors(void);

void startOutput(void);
void stopOutput(void);

#endif // CONTROL_SIGNAL_H
