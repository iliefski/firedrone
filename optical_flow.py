import smbus2
import time
import struct
from math import cos
from math import sin
import curses

# Defines
ADDRESS = 0x42          #px4flow sensors adress
BUSNO = 1               #Sitter sensorn pa bus 1 eller 0
PX4FLOW_TIMEOUT = 0.01  #10ms timeout mellan reads

# Filter constants, calculated in MatLab
a0 = 4.518446191616110
a1 = -7.968417265916514
a2 = 3.513136542467376
b0 = 0.015791367041743
b1 = 0.031582734083486
b2 = 0.015791367041743
QUALITY_THRESHOLD = 125	#Threshold for throwing out measured velocity

# Old filter values
x_filt0 = 0.0
x_filt1 = 0.0
y_filt0 = 0.0
y_filt1 = 0.0
x_real0 = 0.0
x_real1 = 0.0
y_real0 = 0.0
y_real1 = 0.0

#Big endian so big data first
def convert_signed(byte2, byte1):
    comb = bytes([byte2]) + bytes([byte1])
    return struct.unpack('>h', comb)[0]

def test_update(bus):
    stdscr = curses.initscr()
    curses.noecho()
    curses.cbreak()
    stdscr.clear()
    try:
        while 1:
            data = update(bus)
            stdscr.addstr(0, 0, 'Frame count: {}'.format(data[0]))
            stdscr.addstr(1, 0, 'X,Y flow: {} , {}'.format(data[1], data[2]))
            stdscr.addstr(2, 0, 'X,Y velo: {} , {}'.format(data[3], data[4]))
            stdscr.addstr(3, 0, 'Qual: {}'.format(data[5]))
            stdscr.addstr(4, 0, 'X,Y,Z gyro: {} , {} , {}'.format(data[6], data[7], data[8]))
            stdscr.addstr(5, 0, 'Range gyro: {}'.format(data[9]))
            stdscr.addstr(6, 0, 'Sonar time: {}'.format(data[10]))
            stdscr.addstr(7, 0, 'Ground dist: {}'.format(data[11]))
            time.sleep(PX4FLOW_TIMEOUT)
            stdscr.refresh()
    except:
        curses.nocbreak()
        curses.echo()
        curses.endwin()
    finally:
        curses.nocbreak()
        curses.echo()
        curses.endwin()

def test_update_int(bus):
    data = update_integral(bus)
    print('-----------------')
    print('Frame count: ', data[0])
    print('X,Y flow int: ', data[1], ',', data[2])
    print('X,Y,Z gyro: ', data[3], ',', data[4], ',', data[5])
    print('int timespan: ', data[6])
    print('sonar timestamp: ', data[7])
    print('ground dist: ', data[8])
    print('Gyro temp: ', data[9])
    print('Qual: ', data[10])

def update(bus):
    bus.write_byte(ADDRESS, 0x00)

    block = bus.read_i2c_block_data(ADDRESS, 0x00, 22)

    #follow https://github.com/eschnou/arduino-px4flow-i2c/blob/master/PX4Flow/PX4Flow.cpp
    frame_count         = (block[1]<<8) + block[0]
    pixel_flow_x_sum    = convert_signed(block[3], block[2])
    pixel_flow_y_sum    = convert_signed(block[5], block[4])
    flow_comp_m_x       = convert_signed(block[7], block[6])
    flow_comp_m_y       = convert_signed(block[9], block[8])
    qual                = (block[11]<<8) + block[10]
    gyro_x_rate         = convert_signed(block[13], block[12])
    gyro_y_rate         = convert_signed(block[15], block[14])
    gyro_z_rate         = convert_signed(block[17], block[16])
    gyro_range          = block[18]
    sonar_timestamp     = block[19]
    ground_distance     = (block[21]<<8) + block[20]

    return (frame_count, pixel_flow_x_sum, pixel_flow_y_sum, flow_comp_m_x, flow_comp_m_y, qual, gyro_x_rate, gyro_y_rate, gyro_z_rate, gyro_range, sonar_timestamp, ground_distance)

# Returns a filtered x velocity, y velocity and height above ground in cm/s
def update_filtered(bus):
	global x_filt0
	global x_filt1
	global y_filt0
	global y_filt1
	global x_real0
	global x_real1
	global y_real0
	global y_real1

	data = update(bus)
	x_real2 = data[3]
	y_real2 = data[4]
	qual = data[5]
	if(qual < QUALITY_THRESHOLD):
		x_real2 = x_real1
		y_real2 = y_real1
	x_filt2 = (0 - a1*x_filt1 - a2*x_filt0 + b0*x_real2 + b1*x_real1 + b2*x_real0)/a0;
	y_filt2 = (0 -a1*y_filt1 - a2*y_filt0 + b0*y_real2 + b1*y_real1 + b2*y_real0)/a0;
	x_filt0 = x_filt1
	x_filt1 = x_filt2
	y_filt0 = y_filt1
	y_filt1 = y_filt2
	x_real0 = x_real1
	x_real1 = x_real2
	y_real0 = y_real1
	y_real1 = y_real2
	return (-x_filt2, -y_filt2, data[11])

def update_integral(bus):
    bus.write_byte(ADDRESS, 0x16)

    block = bus.read_i2c_block_data(ADDRESS, 0x16, 26)

    frame_count_since_last_read = (block[1]<<8) + block[0]
    pixel_flow_x_integral  = convert_signed(block[3], block[2])
    pixel_flow_y_integral  = convert_signed(block[5], block[4])
    gyro_x_rate_integral   = convert_signed(block[7], block[6])
    gyro_y_rate_integral   = convert_signed(block[9], block[8])
    gyro_z_rate_integral   = convert_signed(block[11], block[10])
    integration_timespan   = (block[15]<<24) + (block[14]<<16) + (block[13]<<8) + block[12]
    sonar_timestamp        = (block[19]<<24) + (block[18]<<16) + (block[17]<<8) + block[16]
    ground_distance        = (block[21]<<8) + block[20]
    gyro_temperature       = (block[23]<<8) + block[22]
    quality                = block[24]

    return (frame_count_since_last_read, pixel_flow_x_integral, pixel_flow_y_integral, gyro_x_rate_integral, gyro_y_rate_integral, gyro_z_rate_integral, integration_timespan, sonar_timestamp, ground_distance, gyro_temperature, quality)


def init_optical_flow():
	# Initiera I2C busen
	return smbus2.SMBus(BUSNO);
