import comp_receive
from comp_save_data import sensorData
import socket
import time
from multiprocessing.pool import ThreadPool
import msvcrt
import math
import numpy as np
import pathlib
from keras.models import load_model
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session

RPIIP = '192.168.137.122'
RPIRECVPORT = 22004


def kbfunc():
    x = msvcrt.kbhit()
    if x:
        ret = ord(msvcrt.getch())
    else:
        ret = 0
    return ret


def to200Range(data):
    dat = round((data + 1) * 100)
    if dat < 0:
        print('Too low value on prediction, truncating')		
        dat = 0
    elif dat > 200:
        print('Too high value on prediction, trucating')
        dat = 200
    return dat


def initNetwork():
    # Needed for certain GPUs
    config = tf.ConfigProto()
    config.gpu_options.per_process_gpu_memory_fraction = 0.5
    config.gpu_options.visible_device_list = "0"
    set_session(tf.Session(config=config))

    # Set this to the path of the network model
    MODEL_PATH = 'model/models/model_model.236-0.1854081.h5'
    model = load_model(MODEL_PATH)

    # Have the model make a dummy prediction (first prediction takes a lot of time to complete)
    current_state = np.zeros((1, 8))
    model.predict(current_state)
    print("Network has been initialized")
    return model


# If needed, adjusts the data predicted by the model to account for high velocities and heights etc.
def calculateNewRCData(predictedRCData, velocities):
    pitch = float(predictedRCData[0][0])
    roll = float(predictedRCData[0][1])
    yaw = float(predictedRCData[0][2])

    if np.sqrt(velocities[0] * velocities[0] + velocities[1] * velocities[1]) > MAX_VELOCITY:  # Avoid high velocities
        if velocities[0] > abs(velocities[1]):
            pitch = -1
        else:
            if velocities[1] > 0:
                roll = -1
            else:
                roll = 1
        print('Velocity too high, slowing down.')

    if velocities[0] < -0.08:  # Avoid flying backwards
        pitch = 0.5
        print('Backwards velocity detected, pitching forward.')

    return [pitch, roll, yaw]


# Here starts the program

pool = ThreadPool(processes=5)
dataSaver = sensorData()

print("Initializing neural network")
model = initNetwork()

inputKey = 0

print("Waiting for input. Only works in windows cmd!")
while inputKey == 0:
    inputKey = kbfunc()

print("Starting receive threads")
comp_receive.receiveData(dataSaver, pool, RPIIP)

time.sleep(1)

socket_send = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

print("Arming motors")
socket_send.sendto('EngineStart'.encode(), (RPIIP, RPIRECVPORT))
time.sleep(6)
print("Start sending data")
socket_send.sendto('DataArriving'.encode(), (RPIIP, RPIRECVPORT))

flags = 0x00
yaw = 0x65
pitch = 0x65
roll = 0x65
height = 0x48

current_state = np.zeros((1,8))

MAX_VELOCITY = 0.75

startTime = time.time()
beginTime = startTime
done = False
networkactive = False
msgindex = 0
folderpath = 'flightdata' + time.strftime("%Y%m%d-%H%M%S")
pathlib.Path(folderpath).mkdir(exist_ok=True)

with open(folderpath + '/ultra.txt', 'w') as ultralog, open(folderpath + '/flow.txt', 'w') as flowlog, open(folderpath + '/thermal.txt', 'w') as thermallog, open(folderpath + '/ai.txt', 'w') as ailog:
    try:
        while not done:
            inputKey = kbfunc()
            #On keypress, order the drone to land automatically
            if inputKey != 0:
                flags = 0x01
                print("Landing ordered")
                done = True

            ultraData = dataSaver.getUltra()
            flowData = dataSaver.getFlow()
            thermalData = dataSaver.getThermal()

            #Logging
            ultralog.write(str(startTime - beginTime) + ', ')
            ultralog.write(str(ultraData) + '\n')
            flowlog.write(str(startTime - beginTime) + ', ')
            flowlog.write(str(flowData) + '\n')
            thermallog.write(str(startTime - beginTime) + ', ')
            thermallog.write(str(thermalData) + '\n')

            # This is the AI:
            current_state[0] = np.array([flowData[0]/1000.0, flowData[1]/1000.0, ((ultraData[0]/100.0)+0.04)/2.4, ((ultraData[1]/100.0)+0.17)/2.4, ((ultraData[2]/100.0)+0.17)/2.4, ((ultraData[3]/100.0)+0.17)/2.4, ((ultraData[4]/100.0)+0.17)/2.4, ((ultraData[5]/100.0)+0.04)/2.4])
            #print('Current state [forward velocity, right velocity, sensors 1-6] is: {0}'.format(current_state[0]))
            predictedRCData = model.predict(current_state)
            newRCData = calculateNewRCData(predictedRCData, [flowData[0]/1000.0, flowData[1]/1000.0])

            #Only activate the network if we have sufficient height to get velocity data
            if flowData[2] > 310 and not networkactive:
                networkactive = True
                print('Network is active!')
            if not networkactive:
                newRCData = [0, 0, 0]
            else:
                ailog.write(str(startTime - beginTime) + ', ')
                ailog.write(str(current_state[0]) + ', ')
                ailog.write(str(newRCData) + '\n\n')

            #print('Prediction done, sending [pitch, roll, yaw] = {0}'.format(newRCData))			
            # Expected order:	 [control-bits, yaw, pitch, roll, height, servo]
            socket_send.sendto(
                bytes([flags, to200Range(newRCData[2]), to200Range(newRCData[0]), to200Range(newRCData[1]), height]),
                (RPIIP, RPIRECVPORT))

            time.sleep(0.1 - (time.time() - startTime))
            startTime = time.time()
            msgindex = msgindex + 1
    finally:
        socket_send.close()
