import optical_flow
import time
import os

flow_sen = optical_flow.init_optical_flow()
file = open("test_optical_data.txt", 'w')

start = time.time()
try:
	while 1:
		data = optical_flow.update_filtered(flow_sen)
		os.system('cls' if os.name == 'nt' else 'clear')
		print('-------------------')
		print('X velo, {: 07.5f}, Y velo, {: 07.5f}, Height: {:04d}'.format(data[0],data[1],data[2]))
		file.write(str(time.time()-start)+','+str(data[0])+','+str(data[1])+','+str(data[2])+'\n')
		time.sleep(optical_flow.PX4FLOW_TIMEOUT)
except:
	raise
	file.close()
	print("Exception :(")
