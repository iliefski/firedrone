#include "pigpio.h"
#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>
#include "rc_controller.h"

#define THR_IN 16
#define YAW_IN 0
#define ROLL_IN 21
#define PITCH_IN 20
#define MAN_IN 12

static float thr_PW = 0;
static float yaw_PW = 0;
static float roll_PW = 0;
static float pitch_PW = 0;
static float man_PW = 0;

static int manual = 0;
static int count = 0;

struct timeval st, et;
struct timeval yaw_st, yaw_et;
struct timeval roll_st, roll_et;
struct timeval pitch_st, pitch_et;
struct timeval man_st, man_et;

int getRCManual(){
  return manual;
}

int getRCThrottle(){
  return (int) ((thr_PW - 1000.0f)/10.0f);
}

int getRCYaw(){
  return (int) ((yaw_PW - 1500.0f)/5.0f);
}

int getRCPitch(){
  return (int) ((pitch_PW - 1500.0f)/5.0f);
}

int getRCRoll(){
  return (int) ((roll_PW - 1500.0f)/5.0f);
}


static void start_man_timer(){
  gettimeofday(&man_st, NULL);
}

static float stop_man_timer(){
  gettimeofday(&man_et, NULL);
  return man_et.tv_usec - man_st.tv_usec;
}

static void *man_f(void *vargp){
  while(1){
    while(gpioRead(MAN_IN) == 0){}
    start_man_timer();
    while(gpioRead(MAN_IN) == 1){}
    man_PW = stop_man_timer();
    if((man_PW < 1500) && (man_PW > 500)){
      if(count>50){
	if(manual == 0){
	  printf("Manual flying triggered from controller \n");
	}
	manual = 1;
      }
      else{
	count++;
      }
      
    }
    else{
      count = 0;
    }
  }
  return NULL;
}

static void start_pitch_timer(){
  gettimeofday(&pitch_st, NULL);
}

static float stop_pitch_timer(){
  gettimeofday(&pitch_et, NULL);
  return pitch_et.tv_usec - pitch_st.tv_usec;
}

static void *pitch_f(void *vargp){
  while(1){
    while(gpioRead(PITCH_IN) == 0){}
    start_pitch_timer();
    while(gpioRead(PITCH_IN) == 1){}
    pitch_PW = stop_pitch_timer();
  }
  return NULL;
}

static void start_roll_timer(){
  gettimeofday(&roll_st, NULL);
}

static float stop_roll_timer(){
  gettimeofday(&roll_et, NULL);
  return roll_et.tv_usec - roll_st.tv_usec;
}

static void *roll_f(void *vargp){
  while(1){
    while(gpioRead(ROLL_IN) == 0){}
    start_roll_timer();
    while(gpioRead(ROLL_IN) == 1){}
    roll_PW = stop_roll_timer();
  }
  return NULL;
}

static void start_thr_timer(){
  gettimeofday(&st, NULL);
}

static float stop_thr_timer(){
  gettimeofday(&et, NULL);
  return et.tv_usec - st.tv_usec;
}

static void *thr_f(void *vargp){
  while(1){
    while(gpioRead(THR_IN) == 0){}
    start_thr_timer();
    while(gpioRead(THR_IN) == 1){}
    thr_PW = stop_thr_timer();
  }
  return NULL;
}

static void start_yaw_timer(){
  gettimeofday(&yaw_st, NULL);
}

static float stop_yaw_timer(){
  gettimeofday(&yaw_et, NULL);
  return yaw_et.tv_usec - yaw_st.tv_usec;
}

static void *yaw_f(void *vargp){
  while(1){
    while(gpioRead(YAW_IN) == 0){}
    start_yaw_timer();
    while(gpioRead(YAW_IN) == 1){}
    yaw_PW = stop_yaw_timer();
  }
  return NULL;
}


void RC_init(){
  gpioSetMode(THR_IN, PI_INPUT);
  gpioSetMode(YAW_IN, PI_INPUT);
  gpioSetMode(ROLL_IN, PI_INPUT);
  gpioSetMode(PITCH_IN, PI_INPUT);
  gpioSetMode(MAN_IN, PI_INPUT);

  pthread_t thr_t, yaw_t, roll_t, pitch_t, man_t;
  pthread_create(&thr_t, NULL, thr_f, NULL);
  //pthread_create(&yaw_t, NULL, yaw_f, NULL);
  pthread_create(&roll_t, NULL, roll_f, NULL);
  pthread_create(&pitch_t, NULL, pitch_f, NULL);
  pthread_create(&man_t, NULL, man_f, NULL);
  
}

