#include <stdio.h>
#include "control_signal.h"

int main(){
  GPIO_init();
  startOutput();
  
  setRoll(-100);
  setPitch(-100);
  setThrottle(0);
  setYaw(-100);
  sleep(5);
  
  setRoll(100);
  setPitch(100);
  setThrottle(100);
  setYaw(100);
  sleep(5);

  setRoll(0);
  setPitch(0);
  setThrottle(0);
  setYaw(0);
  while(1){}

  stopOutput();
  
  return 0;
}
