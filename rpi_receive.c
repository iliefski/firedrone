#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <time.h>
#include "control_signal.h"

#define PORTNUM 22004
#define TIMEOUT_INTERVAL 600000	//The timeout with no received messages before the drone lands. In microseconds
#define UPDATE_TIME	50000		//The loop time for updating the drone values in microseconds

float rolloffset = 15.75f;
float pitchoffset = -15.5f;
//8.8f innan nya sensorer monterade;

int connsocketfd, socketfd, clientlength;
int msglen;
int done = 0;
int landing_drone = 0;		//Is the drone supposed to land now?
char set_height = 0;
int missed_heights = 0;
int groundclose = 0;
float pitch = 0.0f;
float roll = 0.0f;
float yaw = 0.0f;
fd_set readfd;
struct timespec lastmsg, looptime, tmptime, comptime;

struct timeval timer;
struct sockaddr_in server_addr, client_addr;

char buf[256];

int get_micro_diff(struct timespec time1, struct timespec time2) {
	return (int)((time1.tv_sec - time2.tv_sec)*1000000 + (time1.tv_nsec - time2.tv_nsec)/1000);
}

float micro_to_sec(int microseconds) {
	return (float)microseconds/1000000.0f;
}

int init() {
	connsocketfd = socket(AF_INET, SOCK_DGRAM, 0);
	if(connsocketfd < 0) {
		printf("Could not open socket\n");
		exit(1);
	}

	memset((char *) &server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(PORTNUM);
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);

	if(bind(connsocketfd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
		printf("Could not bind socket to port. Probably locked\n");
		exit(1);
	}

	clientlength = sizeof(client_addr);

	timer.tv_sec = 0;
	timer.tv_usec = 0;

	recv(connsocketfd, buf, sizeof buf, 0);
	printf("rpi_receive got: %s\n", buf);

	GPIO_init(UPDATE_TIME/1000000.0f);

	armMotors();
	clock_gettime(CLOCK_MONOTONIC, &lastmsg);
	clock_gettime(CLOCK_MONOTONIC, &looptime);

	recv(connsocketfd, buf, sizeof buf, 0);
	printf("rpi_receive got (2): %s\n", buf);

	return 0;
}

int run(short height, float x_speed, float y_speed) {
	if(height < 300) {
		height = 3000;
	}
	if(landing_drone) {
		setYaw(0);
		setPitch(pitchoffset);
		setRoll(rolloffset);
		clock_gettime(CLOCK_MONOTONIC, &tmptime);
		if(height < 3000) {
			if(height < 310) {
				groundclose = groundclose + 1;
			}
			if(groundclose < 3) {
				updateThrottle(0.0f, (float)height/(float)1000, micro_to_sec(get_micro_diff(tmptime, looptime)));
			} else {
				setThrottle(((float)getThrottle() - 1000.0f)/10.0f - 1.5f);
			}
		} else  {
			setThrottle(0.998f*((float)getThrottle() - 1000.0f)/10.0f);
		}
	} else {
		FD_ZERO(&readfd);
		FD_SET(connsocketfd,&readfd);
		if(select(connsocketfd+1,&readfd,NULL,NULL,&timer) == -1) {
			landing_drone = 1;
			printf("Error in the connection, landing!\n");
		}
		if(FD_ISSET(connsocketfd,&readfd)) {
			clock_gettime(CLOCK_MONOTONIC, &lastmsg);
			if((msglen = recv(connsocketfd, buf, sizeof buf, 0)) <= 0) {
				landing_drone = 1;
				if(msglen == 0) {
					printf("Socket closed for some reason, landing!\n");
				} else {
					printf("Unkown socket error, landing!\n");
				}
			}
			//Expected order: 	[control-bits, yaw, pitch, roll, height]
			//					Values for yaw, pitch, roll, height is 
			//					bytes between 0 and 200 mapping from -100 to 100
			//	Height:			height is in cm
			//	Control-bit:	0x01 -> Land drone
			//	Control-bit:	0x02 -> Activate servo
			yaw = (float)(buf[1]-100)*0.21f;
			pitch = (float)(buf[2]-100)*0.14f;
			roll = (float)(buf[3]-100)*0.14f;
			set_height = buf[4];
			printf("Yaw %05.2f, pitch %05.2f, roll %05.2f\n", yaw, pitch, roll);
			if(buf[0] & 0x01) {
				printf("Got sent signal to land, landing!\n");
				landing_drone = 1;
			}
			if(buf[0] & 0x02) {
				printf("Activating servo\n");
				setServo(100);
			} else {
				setServo(0);
			}
		} else {
			//If we have not received a message in TIMEOUT_INTERVAL us
			clock_gettime(CLOCK_MONOTONIC, &tmptime);
			if(get_micro_diff(tmptime, lastmsg) > TIMEOUT_INTERVAL) {
				landing_drone = 1;
				printf("Missed too many messages, landing!\n");
			}
		}
		clock_gettime(CLOCK_MONOTONIC, &tmptime);
		//updatePitch(0.0f, x_speed, micro_to_sec(get_micro_diff(tmptime, looptime)));
		//updateRoll(0.0f, y_speed, micro_to_sec(get_micro_diff(tmptime, looptime)));
		setPitch(pitch + pitchoffset);
		setRoll(roll + rolloffset);
		setYaw(yaw);
		updateThrottle((float)set_height/(float)100, (float)height/(float)1000, micro_to_sec(get_micro_diff(tmptime, looptime)));
	}
	clock_gettime(CLOCK_MONOTONIC, &looptime);
	tmptime.tv_sec = 0;
	tmptime.tv_nsec = (UPDATE_TIME - get_micro_diff(looptime,comptime))*1000;
	nanosleep(&tmptime, NULL);
	clock_gettime(CLOCK_MONOTONIC, &comptime);

	if(done) {
		close(connsocketfd);
	}

	return 0;
}
