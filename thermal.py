from Adafruit_AMG88xx import Adafruit_AMG88xx
import time
import curses

curses_init = False

def display_temp(temps, stdscr):
	for i in range(0,8):
		str=''
		for j in range(0,8):
			it = 7-i
			if temps[j*8 + it] < 25:
				temps[j*8 + it] = 0
			str = '{:<5} '.format(temps[j*8 + it]) + str
		stdscr.addstr(i, 0, str)
	stdscr.refresh()

def init_curses():
	screen = curses.initscr()
	curses.noecho()
	curses.cbreak()
	return screen

def close_curses():
	curses.nocbreak()
	curses.echo()
	curses.endwin()

def init_sensor():
	sensor = Adafruit_AMG88xx()
	time.sleep(1)
	return sensor

def get_temps(sensor):
	return sensor.readPixels()

#sens = init_sensor()
#while True:
#	display_temp(get_temps(sens))
