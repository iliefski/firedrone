import thermal

sens = thermal.init_sensor()
stdscr = thermal.init_curses()
try:
	while True:
		thermal.display_temp(thermal.get_temps(sens),stdscr)
finally:
	thermal.close_curses()
