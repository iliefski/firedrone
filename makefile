CC=gcc

dronecontrol: rpi_receive.c control_signal.c
	$(CC) -Wall -pthread -o control_signal.o -c -fPIC control_signal.c -lpigpio -lrt
	$(CC) -Wall -pthread -o rpi_receive.o -c -fPIC rpi_receive.c -lpigpio -lrt
	$(CC) -Wall control_signal.o rpi_receive.o -shared -o dronecontrol.so -lpigpio

dronecontrol_branch: branch_rpi_receive.c control_signal.c
	$(CC) -Wall -pthread -o control_signal.o -c -fPIC control_signal.c -lpigpio -lrt
	$(CC) -Wall -pthread -o rpi_receive.o -c -fPIC branch_rpi_receive.c -lpigpio -lrt
	$(CC) -Wall control_signal.o rpi_receive.o -shared -o dronecontrol.so -lpigpio
